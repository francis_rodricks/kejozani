package com.kejozani.kafkaStreams.basics;

import com.kejozani.kafka.basics.ProducerSendMessage;
import org.apache.kafka.clients.producer.*;

import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerStream {
    public static void main(String[] args)  throws  Exception{
        ProducerStream p = new ProducerStream();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer" );
        Producer<String,String> producer = new KafkaProducer<String,String>(p);
        ProducerRecord<String,String> rec = new ProducerRecord("stream1_basic",null,"this is line one" );
        Future<RecordMetadata> rm = producer.send(rec);
        rec = new ProducerRecord("input-wc-stream",null,"this is line tow" );
        rm = producer.send(rec);
        rec = new ProducerRecord("input-wc-stream",null,"this is line three" );
        rm = producer.send(rec);
        RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
        System.out.println("topic=" + m.topic() + ", offset="+m.offset()+", parttion="+m.partition()+",toString="+m.serializedValueSize());
    }
}
