package com.kejozani.kafka.schemaRegistry.naming_tp_rec;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class PersonNameTP1Avro {
    private  String name_name_rec1;
    private  int name_age_rec1;

}
