package com.kejozani.kafka.schemaRegistry.naming_tp_rec;


import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.*;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerNameTP1Message {
    public static void main(String[] args)  throws  Exception{
        ProducerNameTP1Message p = new ProducerNameTP1Message();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName() );
        p.put("schema.registry.url","http://localhost:8081/");
    //    p.put("value.subject.name.strategy", TopicNameStrategy.class.getName());
       // p.put("auto.register.schemas","false");

        p.put("value.subject.name.strategy","io.confluent.kafka.serializers.subject.TopicRecordNameStrategy");
//        UserJson user = new UserJson();
//        user.setName("tom");
//        user.setAge(1);
        Producer<String, GenericRecord> producer = new KafkaProducer( p );

        Schema.Parser parser = new Schema.Parser();
        Schema schema1 = parser.parse(new File("src/main/resources/kyc_name2_tp_rec1.avsc"));
        Schema schema2 = parser.parse(new File("src/main/resources/kyc_name2_tp_rec2.avsc"));
        Schema schema3 = parser.parse(new File("src/main/resources/kyc_name2_tp_rec3.avsc"));

        GenericRecord genRec1 = new GenericData.Record(schema1);
        GenericRecord genRec2 = new GenericData.Record(schema2);
        GenericRecord genRec3 = new GenericData.Record(schema3);
        //genRec1
        genRec1.put("name_name_rec1","tom_avr1");
        genRec1.put("name_age_rec1",10);
        ProducerRecord<String, GenericRecord> rec1 = new ProducerRecord("kyc_name2",null, genRec1 );
        Future<RecordMetadata> rm = producer.send(rec1);
        //genRec2
        genRec2.put("name_name_rec2","tom_avr2");
        genRec2.put("name_age_rec2",20);
        ProducerRecord<String, GenericRecord> rec2 = new ProducerRecord("kyc_name2",null, genRec2 );
        Future<RecordMetadata> rm2 = producer.send(rec2);
//        //genRec3
        genRec3.put("name_name_rec3","tom_avr3");
        genRec3.put("name_age_rec3",30);
        ProducerRecord<String, GenericRecord> rec3 = new ProducerRecord("kyc_name2",null, genRec3 );
        Future<RecordMetadata> rm3 = producer.send(rec3);

        RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
        System.out.println("topic=" + m.topic() + ", offset="+m.offset()+", parttion="+m.partition()+",toString="+m.serializedValueSize());
    }
}
