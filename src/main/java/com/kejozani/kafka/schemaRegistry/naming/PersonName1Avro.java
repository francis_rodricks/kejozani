package com.kejozani.kafka.schemaRegistry.naming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class PersonName1Avro {
    private  String name_name1;
    private  int name_age1;

    private  int name_age2;
}
