package com.kejozani.kafka.schemaRegistry.naming;


import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.*;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerName1Message {
    public static void main(String[] args)  throws  Exception{
        ProducerName1Message p = new ProducerName1Message();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName() );
        p.put("schema.registry.url","http://localhost:8081/");
    //    p.put("value.subject.name.strategy", TopicNameStrategy.class.getName());
        p.put("auto.register.schemas","false");
//        UserJson user = new UserJson();
//        user.setName("tom");
//        user.setAge(1);
        Producer<String, GenericRecord> producer = new KafkaProducer( p );

        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(new File("src/main/resources/kyc_name2_tp_rec1.avsc.avsc"));
        GenericRecord genRec = new GenericData.Record(schema);
        genRec.put("name_name_rec1","tom_avr1");
        genRec.put("name_age_rec1",10);
        ProducerRecord<String, GenericRecord> rec = new ProducerRecord("kyc_name2",null, genRec );
        Future<RecordMetadata> rm = producer.send(rec);

        RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
        System.out.println("topic=" + m.topic() + ", offset="+m.offset()+", parttion="+m.partition()+",toString="+m.serializedValueSize());
    }
}
