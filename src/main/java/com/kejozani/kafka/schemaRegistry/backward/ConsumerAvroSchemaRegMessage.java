package com.kejozani.kafka.schemaRegistry.backward;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kejozani.kafka.dataFormat.avro.PersonAvro;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ConsumerAvroSchemaRegMessage {
    private final ObjectMapper objectMapper = new ObjectMapper();
    public static void main(String[] args) throws IOException {
        ConsumerAvroSchemaRegMessage p = new ConsumerAvroSchemaRegMessage();
        p.getMesage();
    }
    public void getMesage() throws IOException {
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        p.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName() );
        p.put(ConsumerConfig.GROUP_ID_CONFIG, "gr-2" );
        p.put("schema.registry.url","http://localhost:8081/");
     //   p.put("auto.register.schemas","false");
     //   p.put("value.subject.name.strategy", TopicNameStrategy.class.getName()  );
                        //"io.confluent.kafka.serializers.subject.strategy.SubjectNameStrategy");
        Consumer<String, PersonSchemaRegConsAvro> consumer = new KafkaConsumer(p);
        List<String> l = Arrays.asList("kyc_back");
        consumer.subscribe(l);
       // CachedSchemaRegistryClient c;
        while(true) {
            ConsumerRecords<String, PersonSchemaRegConsAvro> recs = consumer.poll(1);
            for (ConsumerRecord rec:recs) {
                /// HOWWWW
                System.out.println("key=" + rec.key() + ",value=" + rec.value() +",tostr="+ rec.toString());
                PersonSchemaRegConsAvro av= objectMapper.readValue( rec.value().toString().getBytes(), PersonSchemaRegConsAvro.class);
                System.out.println("from object av=="+av.toString());

            }
        }
    }
}
