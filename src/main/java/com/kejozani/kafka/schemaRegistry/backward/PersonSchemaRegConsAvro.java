package com.kejozani.kafka.schemaRegistry.backward;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class PersonSchemaRegConsAvro {
    private  String name1;
    //private int age1;
    private  int age12;
    private  int age13;
    private  int age14;

}
