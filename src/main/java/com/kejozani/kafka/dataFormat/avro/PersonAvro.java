package com.kejozani.kafka.dataFormat.avro;

import lombok.Data;

@Data
public class PersonAvro {
    private String name1;
    private int age1;
    private String age12;

}
