package com.kejozani.kafka.dataFormat.avro;


import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.*;

import javax.inject.Singleton;
import java.io.File;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerAvroMessage {
    public static void main(String[] args)  throws  Exception{
        ProducerAvroMessage p = new ProducerAvroMessage();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName() );
        p.put("schema.registry.url","http://localhost:8081/");
    //    p.put("value.subject.name.strategy", TopicNameStrategy.class.getName());
      //  p.put("auto.register.schemas","false");

//        UserJson user = new UserJson();
//        user.setName("tom");
//        user.setAge(1);
        Producer<String, GenericRecord> producer = new KafkaProducer( p );

        //@Singleton
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(new File("src/main/resources/person1.avsc"));
        GenericRecord genRec = new GenericData.Record(schema);

        genRec.put("name1","tom_avr");
        genRec.put("age1",10);
        genRec.put("age12","10str");

        ProducerRecord<String, GenericRecord> rec = new ProducerRecord("kyc_client1",null, genRec );
        Future<RecordMetadata> rm = producer.send(rec);

        RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
        System.out.println("topic=" + m.topic() + ", offset="+m.offset()+", parttion="+m.partition()+",toString="+m.serializedValueSize());
    }
}
