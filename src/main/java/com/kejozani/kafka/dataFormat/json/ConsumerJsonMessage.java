package com.kejozani.kafka.dataFormat.json;

import com.kejozani.kafka.util.KafkaUtils;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ConsumerJsonMessage {
    public static void main(String[] args) {
        ConsumerJsonMessage p = new ConsumerJsonMessage();
        p.getMesage();
    }
    public void getMesage(){
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        p.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, PersonJsonDeSerialize.class.getName() );
        p.put(ConsumerConfig.GROUP_ID_CONFIG, "gr-1" );

        Consumer<String, UserJson> consumer = new KafkaConsumer(p);
        List<String> l = Arrays.asList("kyc_client1");
        consumer.subscribe(l);
        while(true) {
            ConsumerRecords<String, UserJson> recs = consumer.poll(1);
            for (ConsumerRecord rec:recs)
            System.out.println("key="+rec.key()
                    + ",value-name="+((UserJson)rec.value()).getName()
                    + ",value-age="+((UserJson)rec.value()).getAge()
                    + "\n,value="+rec.toString() );
        }
    }
}
