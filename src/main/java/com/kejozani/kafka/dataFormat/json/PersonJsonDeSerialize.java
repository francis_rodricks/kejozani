package com.kejozani.kafka.dataFormat.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import java.io.IOException;

public class PersonJsonDeSerialize implements Deserializer<UserJson> {
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public UserJson deserialize(String topic, byte[] data) {
        if (data == null)
            return null;
        try {
            return objectMapper.readValue(data, UserJson.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {

    }
}
