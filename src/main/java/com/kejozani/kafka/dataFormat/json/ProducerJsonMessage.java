package com.kejozani.kafka.dataFormat.json;

//import io.confluent.kafka.serializers.json.KafkaJsonSchemaSerializer;
import org.apache.kafka.clients.producer.*;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerJsonMessage {
    public static void main(String[] args)  throws  Exception{
        ProducerJsonMessage p = new ProducerJsonMessage();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, PersonJsonSerialize.class.getName() );
        UserJson user = new UserJson();
        user.setName("tom");
        user.setAge(1);
        Producer<String, UserJson> producer = new KafkaProducer( p );
        ProducerRecord<String,UserJson> rec = new ProducerRecord("kyc_client1",null,user );
        Future<RecordMetadata> rm = producer.send(rec);
        RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
        System.out.println("topic=" + m.topic() + ", offset="+m.offset()+", parttion="+m.partition()+",toString="+m.serializedValueSize());
    }
}
