package com.kejozani.kafka.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

//comma-delimter string, pass ordered header ONLY
public class CsvToJSONConvertor {
    private List<String> headerColsList =new ArrayList<String>();;
    private String delimiter=",";
    private static final String QUOTE= "\"";
    public static void main(String[] args) {
//        CsvToJSONConvertor csv = new CsvToJSONConvertor("name,age,sal",",");
//        csv.getJSONStr("tom,10,aaa");
//        csv.getJSONStr("tom,36,aaa");

        String headerStr="year,industry_code_ANZSIC,industry_name_ANZSIC,rme_size_grp,variable,value,unit";
        CsvToJSONConvertor csv1 = new CsvToJSONConvertor(headerStr,",");
        csv1.getJSONStr("2011,A,Agriculture,a_0,Activity unit,46134,COUNT");

    }
    public CsvToJSONConvertor(String fileHeaderCommaDelimted, String delim){
        if (fileHeaderCommaDelimted == null && fileHeaderCommaDelimted.trim().length() >0){
            throw new IllegalArgumentException("File Header is blank. At least one column must be present");
        }
        StringTokenizer st = new StringTokenizer(fileHeaderCommaDelimted,delim);
        while ( st.hasMoreTokens()){
            this.headerColsList.add(st.nextToken());
        }
       // System.out.println("headerColsList="+ headerColsList.size());
    }
    public String getJSONStr(String jsonData){
        StringTokenizer st = new StringTokenizer(jsonData,delimiter);
        int i=0;
        StringBuilder finalJsonStr = new StringBuilder("{");
        while ( st.hasMoreTokens()){
           // System.out.println("i=="+i);
            String header= headerColsList.get(i);
            String value = st.nextToken().trim().replaceFirst("\"","");
            if (i==0){
                finalJsonStr.append(QUOTE + header + QUOTE +":" + QUOTE + value + QUOTE);
            }else{
                finalJsonStr.append(","+QUOTE + header + QUOTE +":" + QUOTE + value + QUOTE);
            }
            i++;
        }
        finalJsonStr.append("}");
        System.out.println("finalJsonStr="+finalJsonStr.toString());
        return finalJsonStr.toString();
    }


}
