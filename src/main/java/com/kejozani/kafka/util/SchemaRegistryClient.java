package com.kejozani.kafka.util;

import io.confluent.kafka.schemaregistry.ParsedSchema;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SchemaRegistryClient {
    private CachedSchemaRegistryClient client;
    public static void main(String[] args) {
        SchemaRegistryClient cl = new  SchemaRegistryClient();
        cl.getAllSubjects();
    }
//subjects
//ids
//versions
    public SchemaRegistryClient(){
        this.client =
                new CachedSchemaRegistryClient(
                        "http://localhost:8081",1000);
    }
    public void getAllSubjects(){
        try {
           Collection<String> subjectList = client.getAllSubjects();
           // subjectList.stream().forEach(System.out::println);
            List<String> subList = subjectList.stream().collect(Collectors.toList());
            for (String sub: subList){
                System.out.println("-----------------------");
                printSubjectVersion(sub);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }
    public void printSubjectVersion(String sub){
        try {
            List<Integer> verList = client.getAllVersions(sub);
            for (Integer version : verList){
                //System.out.println("--------");
                //System.out.println("subject="+sub +", version="+ version);
                printSchemaString(sub,version);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }
    public void printSchemaString(String subject, int version){
        try {
            // schema IDs have distinct avro schemas
            // the same schema id can be used with diff subject (related to a topic) and versions
            ParsedSchema schema= client.getSchemaBySubjectAndId(subject, version);
            System.out.println("--------");
            //client.getCompatibility()
            System.out.println("subject="+subject +", version="+ version+", comp="+client.getCompatibility("Kafka1234-value") );

            System.out.printf("id=%s,name=%s,schemaType=%s,canonicalString=%s,ref=%s,rawSchema=%s,format=%s "
                    ,client.getSchemaMetadata(subject,version).getId(), schema.name(),schema.schemaType(),schema.canonicalString()
                    , schema.references(),schema.rawSchema(),schema.formattedString("") );
            System.out.println("\n--------");
         } catch (IOException e) {
            e.printStackTrace();
        } catch (RestClientException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

}
