package com.kejozani.kafka.util;

import kafka.Kafka;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaUtils {
    private Properties p = null;
    public KafkaUtils(){
        p = new Properties();
    }
    public void setProperties(String key, String value){
        this.p.put(key,value);
    }
    public Properties getPropertiesDef(){
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName() );
        //add all default properties
        //commit, default batch, acks
        return p;
    }
    public  Producer getKafkaProducerDef(){
        Producer producer = new KafkaProducer( getPropertiesDef());
        return producer;
    }
    public  Consumer getKafkaConsumerDef(){
        Consumer consumer = new KafkaConsumer(getPropertiesDef());
        return consumer;
    }
}
