package com.kejozani.kafka.util;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

@ToString
@Data
// ONLY set with Kafka default properties
public class KafkaSetupDTO {
    public String bootstrapServers = "localhost:9092";
    private String strSerializer = StringSerializer.class.getName();
    private String strDeserializer = StringDeserializer.class.getName();

    private String jsonSerializer = StringSerializer.class.getName();

}
