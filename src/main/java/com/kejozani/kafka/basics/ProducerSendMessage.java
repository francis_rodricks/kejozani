package com.kejozani.kafka.basics;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.clients.producer.Producer;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerSendMessage {

    public static void main(String[] args)  throws  Exception{
        ProducerSendMessage p = new ProducerSendMessage();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        //p.put("schema.registry.url","http://localhost:8081/")
        Producer<String,String> producer = new KafkaProducer<String,String>(p);

        ProducerRecord<String,String> rec = new ProducerRecord("kyc_client1",null,"hello="+new Date().getTime() );
        Future<RecordMetadata> rm = producer.send(rec);
        RecordMetadata m = rm.get(1, TimeUnit.SECONDS);
        System.out.println("topic=" + m.topic() + ", offset="+m.offset()+", parttion="+m.partition()+",toString="+m.serializedValueSize());
    }
}


