package com.kejozani.kafka.basics;

import org.apache.kafka.clients.consumer.*;

import java.util.*;

public class ConsumerGetMessage {
    public static void main(String[] args) {
        ConsumerGetMessage p = new ConsumerGetMessage();
        p.getMesage();
    }
    public void getMesage(){
        Properties p = new Properties();
        p.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        p.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer" );
        p.put(ConsumerConfig.GROUP_ID_CONFIG, "gr-1" );
        //p.put("schema.registry.url","http://localhost:8081/");

        Consumer<String,String> consumer =  new KafkaConsumer(p);
        List<String> l = Arrays.asList("kyc_client1");
        consumer.subscribe(l);
        while(true) {
            ConsumerRecords<String,String> recs = consumer.poll(1);
            for (ConsumerRecord rec:recs)
            System.out.println("key="+rec.key() + ",value="+rec.value() );
        }
    }
}
