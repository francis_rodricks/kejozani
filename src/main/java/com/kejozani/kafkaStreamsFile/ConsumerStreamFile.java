package com.kejozani.kafkaStreamsFile;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;

//stream1_basic
//stream1_basic_out
public class ConsumerStreamFile {
    public static final String INPUT_TOPIC = "stream1_basic";
    public static final String OUTPUT_TOPIC = "stream1_basic_out";
    static Properties getStreamsConfig() {
        final Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-file-read");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }
    static void readAllRows(final StreamsBuilder builder) {
        System.out.println("createWordCountStream1");
        final KStream<String, String> source = builder.stream(INPUT_TOPIC);
        System.out.println("START:readAllRows");
        //System.out.println("input val="+ source.toString());
//        final KTable<String, Long> counts = source
//                .flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split(" ")))
//                .groupBy((key, value) -> key+","+value)
//                .count();
//        //source.to();
        //System.out.println("counts="+counts.toString());
        // need to override value serde to Long type
        //System.out.println("count="+counts);
        //source.toStream().to(OUTPUT_TOPIC, Produced.with(Serdes.String(), Serdes.Long()));
        source.filter( (s,b) -> s.length() >100);
        source.to(OUTPUT_TOPIC); //, Produced.with(Serdes.String(), Serdes.Long()));
        System.out.println("END:readAllRows finalf");
    }
    public static void main(final String[] args) {
        final Properties props = getStreamsConfig();
        System.out.println("start1");
        final StreamsBuilder builder = new StreamsBuilder();
        readAllRows(builder);
        System.out.println("start2");

        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        try {
            System.out.println("start3");

            streams.start();
        } catch (final Throwable e) {
            e.printStackTrace();
           // System.exit(1);
        }
        System.out.println("end");
        //System.exit(0);
    }
}