package com.kejozani.kafkaStreamsFile;

import org.apache.kafka.clients.consumer.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class ConsumerGetStreamMessageFile {
    public static void main(String[] args) {
        ConsumerGetStreamMessageFile p = new ConsumerGetStreamMessageFile();
        p.getMesage();
    }
    public void getMesage(){
        Properties p = new Properties();
        p.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        p.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer" );
        p.put(ConsumerConfig.GROUP_ID_CONFIG, "gr-stream-1-FILE" );
        //p.put("schema.registry.url","http://localhost:8081/");

        Consumer<String,String> consumer =  new KafkaConsumer(p);
        List<String> l = Arrays.asList("stream1_basic_out");
        consumer.subscribe(l);
        while(true) {
            ConsumerRecords<String,String> recs = consumer.poll(1);
            for (ConsumerRecord rec:recs) {
                System.out.println("date=" + new Date().getTime() + ", key=" + rec.key() + ",value=" + rec.value());
            }
        }
    }
}
