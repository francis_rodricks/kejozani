package com.kejozani.kafkaStreamsFile;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.Serdes;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerStreamFile {
    public static void main(String[] args)  throws  Exception{
        ProducerStreamFile p = new ProducerStreamFile();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer" );
        //Serdes.Person().getClass();
        Producer<String,String> producer = new KafkaProducer<String,String>(p);
        String fileName= "C:\\_kejozani\\1_kafka\\_docs\\annual_stream1.csv";
        List<String> rowList = Files.readAllLines(Paths.get(
                new File("C:\\_kejozani\\1_kafka\\_docs\\annual_stream1.csv").toURI()));
            Future<RecordMetadata> rm = null;
            int i = 0;
            for (String rowOne : rowList) {
                System.out.println("rowONw="+rowOne);
                i++;
                ProducerRecord<String, String> rec = new ProducerRecord("stream1_basic", i+"", rowOne);
                rm = producer.send(rec);
            }
            RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
            System.out.println("topic=" + m.topic() + ", offset=" + m.offset() + ", parttion=" + m.partition() + ",toString=" + m.serializedValueSize());
        }
}
