package com.kejozani.StreamsObj;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@Getter @Setter
public class Occ implements Serializable {
  private String year;
  private String industry_code_ANZSIC;
  private String industry_name_ANZSIC;
  private String rme_size_grp;
  private String variable;
  private String value;
  private String unit;
}

