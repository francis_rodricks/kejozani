package com.kejozani.StreamsObj;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kejozani.kafka.util.CsvToJSONConvertor;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//stream1_basic
//stream1_basic_out
public class ConsumerStreamObj_Back2 {
    public static final String INPUT_TOPIC = "stream1_basic";
    public static final String OUTPUT_TOPIC = "stream1_basic_out";
    private static final ObjectMapper objMapper = new ObjectMapper();
    static Properties getStreamsConfig() {
        final Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-file-readOBJ");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        return props;
    }
    final  static String headerStr="year,industry_code_ANZSIC,industry_name_ANZSIC,rme_size_grp,variable,value,unit";
    static void readAllRows(final StreamsBuilder builder) {
        System.out.println("createWordCountStream1");
        final KStream<String, String> source = builder.stream(INPUT_TOPIC);
        System.out.println("START:readAllRows" +source);
        source.mapValues((k,v) -> { //String, Occ
            String jsonStr = "";
            Occ o = null;
            try {
                jsonStr = new CsvToJSONConvertor(headerStr, ",").getJSONStr(v);
                o = objMapper.readValue(jsonStr, Occ.class);
                // System.out.println("occ OBJ="+o.toString());
            } catch (Exception e) {
                System.out.println("ignoreeeeeeeeeeeeeeeeeeeeeeeeeeee" + e.getMessage());
            }
            return o;
        }).map( (k,v) -> { //String, Occ
                return new KeyValue<String,Occ>(k,v);
        }).flatMapValues( (k,v) -> { //String, Occ
            //Set or List
            List<Occ> oL = new ArrayList<Occ>();
            oL.add( v );
            return oL;
        }).flatMap( (k,v) -> { //String, Occ
            //Set or List
            List<KeyValue<String,Occ>> oL = new ArrayList<>();
            oL.add( new KeyValue<String,Occ>(k,v) );
            return oL;
        }).flatMapValues( (k,v) -> { //String, Occ
            //Set or List
            List<Occ> oL = new ArrayList<>();
            oL.add( v );
            return oL;
        }).filterNot(
                (k,v)->{
                        try {
                           return Integer.parseInt(v.getValue()) < 500000;
                        }catch (Exception e){
                             System.out.println("ignore filterrrrrrrr"+v);
                        }
                        return false;
        }).peek((k,v)->{
            System.out.println("peeeeeeekingggggggg............................ k="+k+","+"v="+v);
        //creating new KEYS triggers a repartition topic -- EXPENSIVEf
        //HAS TO CALCULATE the partition to place the message
        //SOOOOOOOOO use the correct mapper ALWAYS
        }).mapValues( (k,v)-> { //String,occ
                try {
                   // System.out.println("occ OBJ vobj to str=" + v);
                    return v.toString();
                }catch (Exception e){
                    System.out.println("ignore============"+v);
                }
                return (v==null)?"":v.toString();
        }).to(OUTPUT_TOPIC);
         System.out.println("END:readAllRows finalf");
    }

    public static void main(final String[] args) {
        final Properties props = getStreamsConfig();
        System.out.println("start1");
        final StreamsBuilder builder = new StreamsBuilder();
        readAllRows(builder);
        System.out.println("start2");

        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        try {
            System.out.println("start3");

            streams.start();
        } catch (final Throwable e) {
            e.printStackTrace();
           // System.exit(1);
        }
        System.out.println("end");
        //System.exit(0);
    }
}