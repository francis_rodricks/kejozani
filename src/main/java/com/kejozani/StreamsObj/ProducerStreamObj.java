package com.kejozani.StreamsObj;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.confluent.kafka.serializers.KafkaJsonDeserializer;
import io.confluent.kafka.serializers.KafkaJsonSerializer;
import org.apache.kafka.clients.producer.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerStreamObj {
    public static void main(String[] args)  throws  Exception{
        ProducerStreamObj p = new ProducerStreamObj();
        p.sendMesage();
    }
    public void sendMesage() throws  Exception{
        Properties p = new Properties();
        p.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        p.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        p.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer" );
        //Serdes.Person().getClass();
        KafkaJsonSerializer<Occ> OccSer = new KafkaJsonSerializer<>();
        KafkaJsonDeserializer<Occ> OccDeser = new KafkaJsonDeserializer<>();
        final ObjectMapper objectMapper = new ObjectMapper();
        Producer<String,String> producer = new KafkaProducer<String,String>(p);
        String fileName= "C:\\_kejozani\\1_kafka\\_docs\\annual_streamJSON.csv";
        List<String> rowList = Files.readAllLines(Paths.get(
                new File("C:\\_kejozani\\1_kafka\\_docs\\annual_streamJSON.csv").toURI()));
            Future<RecordMetadata> rm = null;
            int i = 0;
            for (String rowOne : rowList) {
              //  Occ o = objectMapper.readValue(rowOne,Occ.class);
                System.out.println("rowONw="+rowOne);
                i++;
                ProducerRecord<String, String> rec = new ProducerRecord("stream1_basic", i+"", rowOne);
                rm = producer.send(rec);
            }
            RecordMetadata m = rm.get(5, TimeUnit.SECONDS);
            System.out.println("topic=" + m.topic() + ", offset=" + m.offset() + ", parttion=" + m.partition() + ",toString=" + m.serializedValueSize());
        }
}
